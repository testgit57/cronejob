FROM golang:1.19.3-alpine
RUN mkdir cronejob
COPY . /cronejob
WORKDIR /cronejob
RUN go mod tidy
RUN go mod vendor
RUN go build -o main cmd/main.go
CMD ./main