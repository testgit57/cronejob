package main

import (
	"database/sql"
	"time"

	"gitlab.com/testgit57/cronejob/cronejob/config"
	c "gitlab.com/testgit57/cronejob/cronejob/config"
	"gitlab.com/testgit57/cronejob/cronejob/email"
	"gitlab.com/testgit57/cronejob/cronejob/pkg/logger"
	"gitlab.com/testgit57/cronejob/cronejob/pkg/models"
	"gitlab.com/testgit57/cronejob/cronejob/storage/postgres"
)

type Cronjob struct {
	Cfg       *config.Config
	Postgres  *postgres.Postgres
	EmailSend *email.EmailSender
	Logger    logger.Logger
}

func main() {

	cfg := c.LoadConfig()
	log := logger.New(cfg.LogLevel, "cronJob")
	emailSend := email.NewEmailSend(cfg, log)

	db, err := postgres.NewPostgres(*cfg)
	if err != nil {
		panic(err)
	}

	defer db.DB.Close()

	cronJob := &Cronjob{
		Cfg:       cfg,
		Postgres:  db,
		EmailSend: emailSend,
		Logger:    log,
	}
	cronJob.Logger.Info(`Cronjob is Working`)

	for {
		schudelMessage, err := cronJob.Postgres.GetScheduledMessages()
		if err != sql.ErrNoRows && err != nil {
			cronJob.Logger.Info(`Error while getting from database`, logger.Error(err))
		}

		for _, msg := range schudelMessage {
			err = cronJob.EmailSend.SendEmailToSupscibers(
				&models.SendEmailConfig{
					Email:    msg.SenderEmail,
					Passwrod: msg.EmailPaassword,
				},
				&models.SendNewsToSupscribersReq{
					To: []*models.Subscriber{
						msg.To,
					},
					News: msg.News,
				})
			if err != nil {
				cronJob.Logger.Info(`Error sending email`, logger.Error(err))
			}
			cronJob.Logger.Info(`Message is sent to` + msg.To.FirstName)
		}
		time.Sleep(time.Minute * 1)
	}

}
