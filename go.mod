module gitlab.com/testgit57/cronejob/cronejob

go 1.19

require (
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.7
	github.com/spf13/cast v1.5.0
	go.uber.org/zap v1.24.0
	golang.org/x/crypto v0.4.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
